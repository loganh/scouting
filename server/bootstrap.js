Meteor.startup(function () {
  if (Teams.find().count() === 0) {

    var teams = [
      {
        number: 3501,
        name: 'Fremont Firebots',
        mascot: 'Firebot',
        fire_level: 99,
      },
      {
        number: 254,
        name: 'Chezy Pofs',
        mascot: 'EJ',
        fire_level: 85,
      },
      {
        number: 1678,
        name: 'Citrus Circuits',
        mascot: 'Orange',
        fire_level: 67,
      },
      {
        number: 670,
        name: 'Homestead Robotics',
        mascot: 'Mustang',
        fire_level: 20,
      },
      {
        number: 8,
        name: 'Paly Robotics',
        mascot: 'Viking',
        fire_level: 20,
      },
    ];

    _.each(teams, function (team) {
      Teams.insert(team);
    });
  }
});
