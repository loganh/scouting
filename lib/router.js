Router.map(function() {
  this.route('home', {
    path: '/',
  });

  this.route('matchView', {
    path: '/match',
  });

  this.route('register', {
    path: '/register',
  });

  this.route('teamList', {
    path: '/teams',
  });

  this.route('newMatch', {
    path: '/new',
  });

  this.route('contact', {
    path: '/contact',
  });
});
