if (Meteor.isClient) {
  var radioInput = function (name) {
    return 'input[name=' + name + ']:checked';
  }

  Template.newMatch.events({
    'submit form': function (event) {
      event.preventDefault();

      var prematch = {
        teamNumber:  $('#team_number').val(),
        matchType:   $('#match_type option:selected').text(),
        matchNumber: $('#match_number').val(),
        noShow:      $('#noshow').is(':checked'),
      };

      var auton = {
        totesInAutoZone:           $(radioInput('auton-totes')).val(),
        stackedInAutoZone:         $('#stacked').is(':checked'),
        droveInAutoZone:           $('#drive').is(':checked'),
        containersInAutoZone:      $(radioInput('auton-containers')).val(),
        autonContainersFromMiddle: $(radioInput('auton-containers-middle')).val(),
        autonOtherActions:         $('#auton_other_actions').val(),
      };

      var teleop = {
        totePickupSpeed:            $(radioInput('tote-speed')).val(),
        coOp:                       $(radioInput('co-op')).val(),
        humanPlayerSkill:           $(radioInput('human-skill')).val(),
        containerPickupSpeed:       $(radioInput('container-speed')).val(),
        robotSpeed:                 $(radioInput('robot-speed')).val(),
        teleopContainersFromMiddle: $(radioInput('teleop-containers-middle')).val(),
        teleopOtherActions:         $('#teleop_other_actions').val(),
      };


    }
  });
}
